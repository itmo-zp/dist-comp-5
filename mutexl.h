#ifndef __IFMO_DISTRIBUTED_CLASS_MUTEXL__H
#define __IFMO_DISTRIBUTED_CLASS_MUTEXL__H

#include "pa2345.h"

int handle_cs_message(const Worker* worker, local_id from, const Message* message, const Message* cs_request);

#endif // __IFMO_DISTRIBUTED_CLASS_MUTEXL__H
