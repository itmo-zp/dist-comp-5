#ifndef __IFMO_DISTRIBUTED_CLASS_PA4__H
#define __IFMO_DISTRIBUTED_CLASS_PA4__H

enum {
    MAX_WORKERS_COUNT = 10
};

int get_mut_exl();

#endif // __IFMO_DISTRIBUTED_CLASS_PA4__H
