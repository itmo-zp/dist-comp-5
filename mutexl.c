#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include "pa5.h"
#include "worker.h"
#include "lamport.h"
#include "ipc.h"
#include "mutexl.h"

typedef struct {
    local_id worker_id;
    timestamp_t timestamp;
} DeferredReply;

typedef struct DeferredReplyNode {
    DeferredReply deferred_reply;
    struct DeferredReplyNode* next;
} DeferredReplyNode;

static DeferredReplyNode* deferred_reply_queue_root = NULL;

static void add_deferred_reply(const local_id worker_id, const timestamp_t timestamp) {
    DeferredReplyNode* deferred_reply_node = malloc(sizeof(DeferredReplyNode));
    *deferred_reply_node = (DeferredReplyNode) {
        .deferred_reply = {
                .worker_id = worker_id,
                .timestamp = timestamp
        },
        .next = NULL
    };

    if (deferred_reply_queue_root == NULL) {
        deferred_reply_queue_root = deferred_reply_node;
        return;
    }
    DeferredReplyNode* node = deferred_reply_queue_root;
    while (node->next != NULL) {
        node = node->next;
    }
    node->next = deferred_reply_node;
}

static int get_deferred_reply(DeferredReply* deferred_reply) {
    if (deferred_reply_queue_root != NULL) {
        DeferredReplyNode* removed_node = deferred_reply_queue_root;
        *deferred_reply = removed_node->deferred_reply;
        deferred_reply_queue_root = removed_node->next;
        free(removed_node);
        return 0;
    }

    return -1;
}

int handle_cs_message(const Worker* worker, local_id from, const Message* message, const Message* cs_request) {
    switch (message->s_header.s_type) {
        case CS_REQUEST:
            if (
                    cs_request == NULL ||
                    cs_request->s_header.s_local_time > message->s_header.s_local_time ||
                    (cs_request->s_header.s_local_time == message->s_header.s_local_time && worker->id > from)
            ) {
                increment_lamport_time();
                Message reply_message = {
                        .s_header = {
                                .s_magic = MESSAGE_MAGIC,
                                .s_payload_len = 0,
                                .s_type = CS_REPLY,
                                .s_local_time = get_lamport_time()
                        }
                };
                send((void*) worker, from, &reply_message);
            } else {
                add_deferred_reply(from, cs_request->s_header.s_local_time);
            }
            break;

        default:
            return -1;
    }

    return 0;
}

int request_cs(const void * self) {
    Worker* worker = (Worker*) self;

    increment_lamport_time();
    Message request_message = {
            .s_header = {
                    .s_magic = MESSAGE_MAGIC,
                    .s_payload_len = 0,
                    .s_type = CS_REQUEST,
                    .s_local_time = get_lamport_time()
            }
    };
    send_multicast(worker, &request_message);

    int received_workers_count = 0;
    int* received_workers = calloc((size_t) worker->worker_pipes_count, sizeof(int));
    Message* received_message = malloc(sizeof(Message));

    for (int i = 0; i < worker->worker_pipes_count; i++) {
        if (get_skipped_message(worker->worker_pipes[i].id, CS_REQUEST, received_message) == 0) {
            handle_cs_message(worker, worker->worker_pipes[i].id, received_message, &request_message);
        }
    }

    while (received_workers_count < worker->worker_pipes_count) {
        for (local_id i = 0; i < worker->worker_pipes_count; i++) {
            if (receive((void *) worker, worker->worker_pipes[i].id, received_message) == 0) {
                timestamp_t received_time = received_message->s_header.s_local_time;
                if (get_lamport_time() < received_time) {
                    set_lamport_time(received_time);
                }
                increment_lamport_time();

                if (received_message->s_header.s_type == CS_REPLY) {
                    if (!received_workers[i]) {
                        received_workers[i] = 1;
                        received_workers_count++;
                    }
                } else if (handle_cs_message(worker, worker->worker_pipes[i].id, received_message, &request_message) != 0) {
                    skip_message(worker->worker_pipes[i].id, received_message);
                }
            }
        }
    }

    free(received_workers);
    free(received_message);

    return 0;
}

int release_cs(const void * self) {
    Worker* worker = (Worker*) self;
    DeferredReply deferred_reply;

    increment_lamport_time();
    while (get_deferred_reply(&deferred_reply) == 0) {
        increment_lamport_time();
        Message reply_message = {
                .s_header = {
                        .s_magic = MESSAGE_MAGIC,
                        .s_payload_len = 0,
                        .s_type = CS_REPLY,
                        .s_local_time = get_lamport_time()
                }
        };
        send((void*) worker, deferred_reply.worker_id, &reply_message);
    }

    return 0;
}
